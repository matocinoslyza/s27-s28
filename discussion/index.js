const protocol = require('http');

// identify and designate a "virtual point" where the network connection will start and end. 
const port = 4000;

// create a connection
protocol.createServer((req, res) => {
// describe the response when the client interacts /communicates with the server.
res.write(`Welcome to the Server`);
res.end();
}).listen(port)
// we will use the listen() method to bind the connection into the designated address 
console.log(`Server is Running on port ${port}`);